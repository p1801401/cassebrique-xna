﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace CasseBriques
{
    public class ObjetAnime
    {
        // On définit les propriétés de l'objet animé
        // Certains objets animés auront une vitesse nulle
        private Texture2D texture;
        private Vector2 position;
        private Vector2 size;
        private Vector2 vitesse;
        private Boolean marque;

        private Rectangle rectangle;
        private BoundingBox bbox;

        public Vector2 Vitesse
        {
            get { return vitesse; }
            set { vitesse = value; }
        }

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        public Vector2 Size
        {
            get { return size; }
            set { size = value; }
        }

        public Boolean Marque
        {
            get { return marque; }
            set { marque = value; }
        }

        public Rectangle Rectangle
        {
            get { return rectangle; }
            set { rectangle = value; }
        }

        public BoundingBox Bbox
        {
            get { return bbox; }
            set { bbox = value; }
        }

        public ObjetAnime(Texture2D texture, Vector2 position, Vector2 size, Vector2 vitesse)
        {
            this.texture = texture;
            this.position = position;
            this.size = size;
            this.vitesse = vitesse;
            this.marque = false;

            this.rectangle = new Rectangle((int)this.position.X, (int)this.position.Y, (int)this.Size.X, (int)this.Size.Y);
            this.bbox = new BoundingBox(new Vector3(this.Position.X, this.Position.Y, 0),
                                        new Vector3(this.Position.X + this.Size.X, this.Position.Y + this.Size.Y, 0));
        }

        public ObjetAnime()
        {
            this.texture = null;
            this.position = new Vector2();
            this.size = new Vector2();
            this.vitesse = new Vector2();
            this.marque = false;

            this.rectangle = new Rectangle((int)this.position.X, (int)this.position.Y, (int)this.Size.X, (int)this.Size.Y);
            this.bbox = new BoundingBox(new Vector3(this.Position.X, this.Position.Y, 0),
                                        new Vector3(this.Position.X + this.Size.X, this.Position.Y + this.Size.Y, 0));
        }



    }

}
