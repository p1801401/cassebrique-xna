using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;


namespace CasseBriques
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Raquette : Microsoft.Xna.Framework.DrawableGameComponent
    {

        SpriteBatch spriteBatch;
        //position initial de la raquette
        private Vector2 position_initiale;
        
        //une raquette est un ObjetAnime
        private ObjetAnime raquette;

        private int maxY, maxX;

        //taille de la raquette
        private int TAILLEBRIQUE_Y = 50;
        private int TAILLEBRIQUE_X = 140;

        public Raquette(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
            this.Game.Components.Add(this);
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            position_initiale = new Vector2(450, 950);
            maxX = 1000;
            maxY = 1000;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            raquette = new ObjetAnime(Game.Content.Load<Texture2D>(@"images\raquette"), position_initiale, new Vector2(TAILLEBRIQUE_X, TAILLEBRIQUE_Y), Vector2.Zero);
            base.LoadContent();
        }
        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            Vector2 position;

            spriteBatch.Begin();

            //si l'action "aller � droite" est d�clench�, on d�place la raquette � droite
            if (Controls.CheckActionDroite())
            {
                position.X = raquette.Position.X + 10 ;
                position.Y = raquette.Position.Y;
                raquette.Position = position;
                spriteBatch.Draw(raquette.Texture, raquette.Position, Color.White);

                //si la raquette d�passe � droite, il se retrouve � gauche de la fenetre
                if (raquette.Position.X > maxX)
                {

                    position.X = 0;
                    raquette.Position = position;
                    spriteBatch.Draw(raquette.Texture, raquette.Position, Color.White);
                }
            }
            //si l'action "aller � gauche" est d�clench�, on d�place la raquette � gauche
            if (Controls.CheckActionGauche())
            {
                position.X = raquette.Position.X - 10;
                position.Y = raquette.Position.Y;

                raquette.Position = position;
                spriteBatch.Draw(raquette.Texture, raquette.Position, Color.White);

                //si la raquette d�passe � gauche, il se retrouve � droite de la fenetre
                if (raquette.Position.X + raquette.Size.X < 0)
                {

                    position.X = maxX;
                    raquette.Position = position;
                    spriteBatch.Draw(raquette.Texture, raquette.Position, Color.White);
                }
            }
                spriteBatch.End();

            raquette.Rectangle = new Rectangle((int)this.raquette.Position.X, (int)this.raquette.Position.Y, (int)this.raquette.Size.X, (int)this.raquette.Size.Y);
            this.raquette.Bbox = new BoundingBox(new Vector3(raquette.Position.X, raquette.Position.Y, 0),
                                        new Vector3(raquette.Position.X + TAILLEBRIQUE_X - 50, raquette.Position.Y + TAILLEBRIQUE_Y, 0));

            base.Update(gameTime);

        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            //on d�sinne la raqutte � sa position
            spriteBatch.Draw(raquette.Texture, raquette.Position, Color.Azure);
            
            spriteBatch.End();
            base.Draw(gameTime);
        }

        public ObjetAnime ObjRaquette
        {
            get { return raquette; }
            set { raquette = value; }
        }

        public int TailleX
        {
            get { return TAILLEBRIQUE_X; }
        }

        public int TailleY
        {
            get { return TAILLEBRIQUE_Y; }
        }

        public Vector2 PositionInitiale
        {
            get { return position_initiale; }
            set { position_initiale = value; }
        }
    }
}
