﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;


namespace CasseBriques
{
    class MoteurJeu
    {

        public const int CROISEMENT = -1; // Si les 2 objets se croisent
        public const int EN_DESSOUS = 0;
        public const int AU_DESSUS = 1;
        public const int A_DROITE = 2;
        public const int A_GAUCHE = 3;

        //fonction testant la collision entre un objet et une boundingbox
        public static Boolean testCollision(Object obj1, BoundingBox obj2)
        {
            // On réalise le mvt dans une bounding box de test et on renvoie le résultat de l'intersection avec l'objet obj
            Vector3 upperLeftCorner = new Vector3(0, 0, 0);
            Vector3 bottomRightCorner = new Vector3(0, 0, 0);

            if (obj1 is Balle)
            {
                Balle balle = (Balle)obj1;
                upperLeftCorner.X = balle.ObjBalle.Position.X + balle.ObjBalle.Vitesse.X;
                upperLeftCorner.Y = balle.ObjBalle.Position.Y + balle.ObjBalle.Vitesse.Y;
                bottomRightCorner.X = balle.ObjBalle.Position.X + balle.ObjBalle.Size.X + balle.ObjBalle.Vitesse.X;
                bottomRightCorner.Y = balle.ObjBalle.Position.Y + balle.ObjBalle.Size.Y + balle.ObjBalle.Vitesse.Y;
            }
            else if (obj1 is Raquette)
            {
                Raquette raquette = (Raquette)obj1;
                upperLeftCorner.X = raquette.ObjRaquette.Position.X;
                upperLeftCorner.Y = raquette.ObjRaquette.Position.Y + raquette.ObjRaquette.Vitesse.Y;
                bottomRightCorner.X = raquette.ObjRaquette.Position.X + raquette.ObjRaquette.Size.X;
                bottomRightCorner.Y = raquette.ObjRaquette.Position.Y + raquette.ObjRaquette.Size.Y + raquette.ObjRaquette.Vitesse.Y;
            }

            BoundingBox bbox_test = new BoundingBox(upperLeftCorner, bottomRightCorner);

            return bbox_test.Intersects(obj2);
        }

        //renvoie la position relative d'un objet par rapport à une référence
        public static int getRelativePosition(float[] obj, float[] reference)
        {
            int positionRel = -1;

            // axe des X
            if (obj[0] > reference[0] + reference[2])
            {
                positionRel = A_DROITE;
            }
            else if (obj[0] + obj[2] < reference[0])
                positionRel = A_GAUCHE;

            // axe des Y
            if (obj[1] > reference[1] + reference[3] + 50)
            {
                positionRel = EN_DESSOUS;
            }
            else if (obj[1] + obj[3] < reference[1])
                positionRel = AU_DESSUS;

            return positionRel;
        }

        //renvoie la position relative de la balle par rapport à la raquette
        public static int getRelativePositionRaquette(float[] obj, float[] reference)
        {
            int positionRel = -1;

            // axe des X
            if (reference[0] + (3 * reference[2] )/ 4 >= obj[0] + obj[2] && obj[0] + obj[2] >= reference[0] + reference[2] / 2)
            {
                positionRel = A_DROITE;

            }
            else if (obj[0] + obj[2] > reference[0] + 3 * reference[2] / 4)
            {
                positionRel = 22;

            }
            else if (reference[0] + reference[2] / 4 < obj[0] + obj[2]  && obj[0] + obj[2] < reference[0] + reference[2] / 2)
            {

                positionRel = A_GAUCHE;
            }
            else if (obj[0] + obj[2] < reference[0] + reference[2] / 4)
            {

                positionRel = 33;
            }

            return positionRel;
        }
    }
}
