using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CasseBriques
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Jeu : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //la police du jeu
        SpriteFont textFont;

        //les diff�rentes briques du jeu
        ObjetAnime briquegrise;
        ObjetAnime briquebleue;
        ObjetAnime briqueorange; 
        ObjetAnime briquepoint;
        ObjetAnime briquerouge;
        ObjetAnime briqueviolet;
        Texture2D unebriquenoire;

        //les diff�rents sons du jeux
        SoundEffect soundLoseBall;
        SoundEffect soundBreaking;

        // taille de la fenetre
        const int TAILLE_W = 1000; 
        const int TAILLE_H = 1000;

        //taille de chaque brique
        const int TAILLEBRIQUE_X = 119;
        const int TAILLEBRIQUE_Y = 50;

        //la balle
        Balle balle;

        //la raquette
        Raquette raquette;

        //nombre de lignes et nombre de briques par ligne
        const int NBLIGNES = 9;
        const int NBBRIQUES = 7;

        //nombre de briques restants dans la partie
        int nb_briques_restant;

        // tableau qui contiendra les briques
        ObjetAnime[,] mesBriques;

        //le background
        Rectangle backgroundRect;
        Texture2D background;

        //le score
        int score;

        public object MouseInput { get; private set; }

        public Jeu()
        {
            graphics = new GraphicsDeviceManager(this);
            //fait en sorte que la souris soit visible lorsqu'elle passe au dessus de la fen�tre de jeu 
            this.IsMouseVisible = true;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            //tableau contenant les briques du jeu
            mesBriques = new ObjetAnime[NBBRIQUES, NBLIGNES];
            //le nombre de briques restants
            nb_briques_restant = NBBRIQUES * NBLIGNES;
            //cr�ation de la balle
            balle = new Balle(this, TAILLE_W, TAILLE_H, 0, 0);
            //creation de la raquette
            raquette = new Raquette(this);

            //on initialise le score � 0
            this.score = 0;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);


            // On initialise la taille de l'�cran de sortie
            graphics.PreferredBackBufferWidth = TAILLE_W;
            graphics.PreferredBackBufferHeight = TAILLE_H;
            graphics.ApplyChanges();


            /// On initialise les diff�rents objets du jeu
            /// lignes de briques
            /// la balle
            /// la raquette
            /// 
            briquegrise = new ObjetAnime(Content.Load<Texture2D>(@"images\briquegrise"), new Vector2(0f, 0f), new Vector2(TAILLEBRIQUE_X, TAILLEBRIQUE_Y), Vector2.Zero);
            briquebleue = new ObjetAnime(Content.Load<Texture2D>(@"images\briquebleue"), new Vector2(0f, 0f), new Vector2(TAILLEBRIQUE_X, TAILLEBRIQUE_Y), Vector2.Zero);
            briqueorange = new ObjetAnime(Content.Load<Texture2D>(@"images\briqueorange"), new Vector2(0f, 0f), new Vector2(TAILLEBRIQUE_X, TAILLEBRIQUE_Y), Vector2.Zero);
            briquepoint = new ObjetAnime(Content.Load<Texture2D>(@"images\briquepoint"), new Vector2(0f, 0f), new Vector2(TAILLEBRIQUE_X, TAILLEBRIQUE_Y), Vector2.Zero);
            briquerouge = new ObjetAnime(Content.Load<Texture2D>(@"images\briquerouge"), new Vector2(0f, 0f), new Vector2(TAILLEBRIQUE_X, TAILLEBRIQUE_Y), Vector2.Zero);
            briqueviolet = new ObjetAnime(Content.Load<Texture2D>(@"images\briqueviolet"), new Vector2(0f, 0f), new Vector2(TAILLEBRIQUE_X, TAILLEBRIQUE_Y), Vector2.Zero);
            unebriquenoire = Content.Load<Texture2D>(@"images\briquenoire");
            //ajout d'un background
            background = Content.Load<Texture2D>(@"images\background");
            backgroundRect = new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
            //ajout des diff�rents soins du jeu
            soundBreaking = Content.Load<SoundEffect>(@"sons\break");
            soundLoseBall = Content.Load<SoundEffect>(@"sons\loseBall");


            int ligne = 80;
            int brique = 50;

            //on ajoute chaque brique dans le tableau mes briques
            // on d�fini arbitrairement leurs positions
            for (int x = 0; x < NBBRIQUES; x++)
            {
                for (int y = 0; y < NBLIGNES; y++)
                {
                    mesBriques[x, y] = new ObjetAnime();
                    switch (y % 5)
                    {
                        case 0:
                            mesBriques[x, y].Texture = briquepoint.Texture;
                            mesBriques[x, y].Position = new Vector2(ligne, brique);
                            break;
                        case 1:
                            mesBriques[x, y].Texture = briquegrise.Texture;
                            mesBriques[x, y].Position = new Vector2(ligne, brique);
                            break;
                        case 2:
                            mesBriques[x, y].Texture = briquerouge.Texture;
                            mesBriques[x, y].Position = new Vector2(ligne, brique);
                            break;
                        case 3:
                            mesBriques[x, y].Texture = briqueorange.Texture;
                            mesBriques[x, y].Position = new Vector2(ligne, brique);
                            break;
                        case 4:
                            mesBriques[x, y].Texture = briqueviolet.Texture;
                            mesBriques[x, y].Position = new Vector2(ligne, brique);
                            break;
                        default:
                            mesBriques[x, y].Texture = briquegrise.Texture;
                            mesBriques[x, y].Position = new Vector2(ligne, brique);
                            break;
                    }
                    brique = brique + TAILLEBRIQUE_Y ;
                }
                ligne = ligne + TAILLEBRIQUE_X;
                brique = TAILLEBRIQUE_Y;

            }



            // On charge la police
            this.textFont = Content.Load<SpriteFont>(@"font\MyFont");


        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            
            // on d�fini la boundingbox de la balle
           this.balle.ObjBalle.Bbox = new BoundingBox(new Vector3(this.balle.ObjBalle.Position.X, this.balle.ObjBalle.Position.Y, 0),
                new Vector3(this.balle.ObjBalle.Position.X + this.balle.ObjBalle.Texture.Width, this.balle.ObjBalle.Position.Y + this.balle.ObjBalle.Texture.Height, 0));

            // on d�fini la boundingbox de chaque brique
            for (int x = 0; x < NBBRIQUES; x++)
            {
                for (int y = 0; y < NBLIGNES; y++)
                {
                    if (mesBriques[x, y].Texture != null)

                        mesBriques[x, y].Bbox = new BoundingBox(new Vector3(this.mesBriques[x, y].Position.X, this.mesBriques[x, y].Position.Y, 0),
                                           new Vector3(this.mesBriques[x, y].Position.X + this.mesBriques[x, y].Texture.Width, this.mesBriques[x, y].Position.Y + this.mesBriques[x, y].Texture.Height, 0));
                }
            }
            //fonction qui g�re les collisions entre la balle et les briques
            gestionCollision();
            //fonction qui g�re les collisions entre la balle et la raquette
            gestionCollisionBalleRaqutte();

            if (balle.balleSortie())
            {
                // si la balle sors de l'�cran par le bas
                int nbBalles = balle.NbBalles;
                balle = null;

                //on d�clenche un son avec un certain volume
                SoundEffectInstance losingBall = soundLoseBall.CreateInstance();
                losingBall.Volume = 0.6f;
                losingBall.Play();

                //cr�antion d'une nouvelle balle
                balle = new Balle(this, TAILLE_W, TAILLE_H, 0, 0);
                balle.NbBalles = nbBalles;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();
            
            //affichage background
            spriteBatch.Draw(background, backgroundRect, Color.Azure);

            //affichage du score
            spriteBatch.DrawString(this.textFont, "Score : " + score , new Vector2(0, 0), Color.White);

            //affichage nombre de vies restant
            spriteBatch.DrawString(this.textFont, "Vies : " + balle.NbBalles , new Vector2(870, 0), Color.White);

           

            // Boucle permettant de dessiner les briques des murs
            for (int x = 0; x < NBBRIQUES; x++)
            {
                for (int y = 0; y < NBLIGNES; y++)
                {
                    if(mesBriques[x,y].Texture != null)
                        spriteBatch.Draw(mesBriques[x,y].Texture, mesBriques[x,y].Position, Color.Azure);
                }
            }

            //la cas o� l'utilisateur n'a plus de balle restant => il a perdu la partie
            if (balle.NbBalles == 0)
            {
                GraphicsDevice.Clear(Color.CornflowerBlue);
                spriteBatch.Draw(background, backgroundRect, Color.Azure);
                spriteBatch.DrawString(this.textFont, "Game Over ... ! Vous avez epuise toutes vos balles", new Vector2(13 * 20 - 50, 20), Color.White);
                balle.ObjBalle.Vitesse = new Vector2(0, 0);
                balle.Visible = false;
                raquette.Visible = false;
                spriteBatch.DrawString(this.textFont, "Appuyez sur 'Entrer' pour recommencer", new Vector2(13 * 20 + 20, 60), Color.White);
            }

            //la cas o� l'utilisateur a cass� toutes les briques => il a gagn� la partie
            if(nb_briques_restant == 0)
            {
                spriteBatch.DrawString(this.textFont, "Bravo !", new Vector2(13 * 20, 700), Color.Blue);


            }
            
            spriteBatch.End();

            //permet de recommencer une partie
            //on peut recommencer une partie � n'importe quel moment
            KeyboardState keyboard = Keyboard.GetState();
            if (keyboard.IsKeyDown(Keys.Enter))
            {
                this.restart();
            }

            base.Draw(gameTime);
        }

        //fonction qui g�re les collisions entre la balle et les briques
        private void gestionCollision()
        {
            Vector2 v;
            //informations sur la position de la balle
            float[] infosBalle = { balle.ObjBalle.Position.X, balle.ObjBalle.Position.Y, balle.ObjBalle.Size.X, balle.ObjBalle.Size.Y };
            int posRel;
            for (int x = 0; x < NBBRIQUES; x++)
            {
                for (int y = 0; y < NBLIGNES; y++)
                {
                    //verifie s'il y a collision entre la balle et une brique
                    if (mesBriques[x, y].Texture != null && MoteurJeu.testCollision(this.balle, mesBriques[x, y].Bbox))
                    {
                        //le cas o� il y a eu collision entre la balle et une brique

                        //informations sur la position de la brique
                        float[] infosBrique = { mesBriques[x, y].Position.X, mesBriques[x, y].Position.Y, mesBriques[x, y].Size.X, mesBriques[x, y].Size.Y };

                        //fonction permettant de savoir d'ou viens la balle
                        //nous allons changer la direction de la balle selon la direction dont elle est issue
                        posRel = MoteurJeu.getRelativePosition(infosBalle, infosBrique);

                        //on incr�mente le score
                        score += 5;

                        // Si les 2 objets se croisent sur l'axe des X
                        if (posRel == MoteurJeu.AU_DESSUS || posRel == MoteurJeu.EN_DESSOUS)
                        {
                            v = balle.ObjBalle.Vitesse;
                            v.Y *= -1;

                            balle.ObjBalle.Vitesse = v;
                        }

                        // Si les 2 objets se croisent sur l'axe des Y
                        if (posRel == MoteurJeu.A_DROITE || posRel == MoteurJeu.A_GAUCHE)
                        {
                            v = balle.ObjBalle.Vitesse;
                            v.X *= -1;
                            balle.ObjBalle.Vitesse = v;
                        }

                        //suppression de la brique
                        mesBriques[x, y].Texture = null;
                        
                        //on d�cr�mente le nombre de briques restant
                        nb_briques_restant--;

                        //on g�n�re un son � chaque collision avec une brique
                        SoundEffectInstance brickBreaking = soundBreaking.CreateInstance();
                        brickBreaking.Volume = 0.6f;
                        brickBreaking.Play();

                    }
                }
            }
        }

        //fonction qui g�re les collisions entre la balle et la raquette
        private void gestionCollisionBalleRaqutte()
        {
            Vector2 v;
            float[] infosBalle = { balle.ObjBalle.Position.X, balle.ObjBalle.Position.Y, balle.ObjBalle.Size.X, balle.ObjBalle.Size.Y };
            int posRel;

            //verifie s'il y a collision entre la balle et une brique
            if (MoteurJeu.testCollision(balle, raquette.ObjRaquette.Bbox))
            {
                //le cas o� il y a eu collision entre la balle et une raquette

                //informations sur la position de la raquette
                float[] infosRaquette1 = { raquette.ObjRaquette.Position.X, raquette.ObjRaquette.Position.Y, raquette.ObjRaquette.Size.X, raquette.ObjRaquette.Size.Y};

                //fonction permettant de savoir d'ou viens la balle
                //nous allons changer la direction de la balle selon la direction dont elle est issue
                posRel = MoteurJeu.getRelativePositionRaquette(infosBalle, infosRaquette1);


                //selon la direction dont viens la balle, on d�cide de la direction vers laquelle elle va se diriger
                switch (posRel)
                {
                    case MoteurJeu.A_DROITE: // posRel = 2 => Droite
                        v = balle.ObjBalle.Vitesse;
                        if (v.X < 0)
                            v.X *= -1;
                        else if (v.X == 0)
                            v.X += 1;
                        else
                            v.X *= 1;
                        v.Y *= -1;
                        balle.ObjBalle.Vitesse = v;
                        break;
                    case MoteurJeu.A_GAUCHE: // posRel = 3 => Gauche
                        v = balle.ObjBalle.Vitesse;
                        if (v.X > 0)
                            v.X *= -1;
                        else if (v.X == 0)
                            v.X += -1;
                        else
                            v.X *= 1;
                        v.Y *= -1;
                        balle.ObjBalle.Vitesse = v;
                        break;
                    case 22: // Droite Droite
                        v = balle.ObjBalle.Vitesse;
                        if (v.X < 0)
                            v.X *= -1.5f;
                        else if (v.X == 0)
                            v.X += 1;
                        else
                            v.X *= 1.5f;

                        v.Y *= -1;
                        if (v.Y < balle.V_max.Y)
                            v.Y *= 1.1f;
                        balle.ObjBalle.Vitesse = v;
                        break;
                    case 33: // Gauche Gauche
                        v = balle.ObjBalle.Vitesse;
                        if (v.X > 0)
                            v.X *= -1.5f;
                        else if (v.X == 0)
                            v.X += -1;
                        else
                            v.X *= 1.5f;

                        v.Y *= -1;
                        if (v.Y < balle.V_max.Y)
                            v.Y *= 1.1f;
                        balle.ObjBalle.Vitesse = v;
                        break;
                    default:
                        v = balle.ObjBalle.Vitesse;
                        v.Y *= -1;
                        balle.ObjBalle.Vitesse = v;
                        break;
                }

            }
        }

        public void restart()
        {
            int ligne = 80;
            int brique = 50;

            //ON remet les briques dans le tableau avec leur position et leur texture
            for (int x = 0; x < NBBRIQUES; x++)
            {
                for (int y = 0; y < NBLIGNES; y++)
                {
                    mesBriques[x, y] = new ObjetAnime();
                    switch (y % 5)
                    {
                        case 0:
                            mesBriques[x, y].Texture = briquepoint.Texture;
                            mesBriques[x, y].Position = new Vector2(ligne, brique);
                            break;
                        case 1:
                            mesBriques[x, y].Texture = briquegrise.Texture;
                            mesBriques[x, y].Position = new Vector2(ligne, brique);
                            break;
                        case 2:
                            mesBriques[x, y].Texture = briquerouge.Texture;
                            mesBriques[x, y].Position = new Vector2(ligne, brique);
                            break;
                        case 3:
                            mesBriques[x, y].Texture = briqueorange.Texture;
                            mesBriques[x, y].Position = new Vector2(ligne, brique);
                            break;
                        case 4:
                            mesBriques[x, y].Texture = briqueviolet.Texture;
                            mesBriques[x, y].Position = new Vector2(ligne, brique);
                            break;
                        default:
                            mesBriques[x, y].Texture = briquegrise.Texture;
                            mesBriques[x, y].Position = new Vector2(ligne, brique);
                            break;
                    }
                    brique = brique + TAILLEBRIQUE_Y;
                }
                ligne = ligne + TAILLEBRIQUE_X;
                brique = TAILLEBRIQUE_Y;

            }

            this.balle.Visible = true;
            this.raquette.Visible = true;

            //on remet la position initiale de la raquette
            this.raquette.ObjRaquette.Position = this.raquette.PositionInitiale;

            //on remet la position initiale de la balle
            this.balle.ObjBalle.Vitesse = this.balle.VitesseInitiale;

            //on r�initialise le score
            this.score = 0;

            //on r�initialise le nombre de balles restants
            this.balle.NbBalles = 3;
        }
    }
}

