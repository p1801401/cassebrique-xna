using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;


namespace CasseBriques
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Balle : Microsoft.Xna.Framework.DrawableGameComponent
    {

        SpriteBatch spriteBatch;

        //position max/min possible de la balle => g�n�ralement �gales aux dimensions de la fenetre de jeu
        private int maxX;
        private int maxY;
        private int minX;
        private int minY;

        //nombre de balles/vies dans une partie
        int nbBalles;

        //vitesse min/mas de la balle 
        private Vector2 v_min;
        private Vector2 v_max;

        //vitesse initiale de la balle
        private Vector2 vitesse_initiale;

        //position initial de la balle
        private Vector2 position_initiale;

        //taille de la balle
        private Vector2 tailleBalle;


        //une balle est un objetAnime
        private ObjetAnime uneBalle;

        public Balle(Game game, int maxX, int maxY, int minX, int minY)
            : base(game)
        {
            this.maxX = maxX;
            this.maxY = maxY;
            this.minX = minX;
            this.minY = minY;

            this.Game.Components.Add(this);
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            
            v_min = new Vector2(0, 4);
            v_max = new Vector2(25, 16);
            //on d�fini la vitesse initiale comme �tant �gale � la vitesse minimale
            vitesse_initiale = v_min;

            position_initiale = new Vector2(450, 650);
            tailleBalle = new Vector2(20, 20);
            this.nbBalles = 3;

            base.Initialize();

        }


        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //on initialise l'ObjetAnime
            uneBalle = new ObjetAnime(Game.Content.Load<Texture2D>(@"images\balle"),
                this.position_initiale, this.tailleBalle, this.vitesse_initiale);

            base.LoadContent();
        }
        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            //on update la bounding box de la balle
            this.ObjBalle.Bbox = new BoundingBox(new Vector3(uneBalle.Position.X, uneBalle.Position.Y, 0),
                new Vector3(uneBalle.Position.X + uneBalle.Texture.Width, uneBalle.Position.Y + uneBalle.Texture.Height, 0));

            //on update la position de la balle
            uneBalle.Position = uneBalle.Position + uneBalle.Vitesse;

            //fonction testant si la balle � touch� le mur de droite, de gauche ou du haut
            collisionMur();
                
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            //on dessine la balle � sa position
            spriteBatch.Draw(uneBalle.Texture, uneBalle.Position, Color.Azure);

            spriteBatch.End();
            base.Draw(gameTime);
        }

        public int NbBalles
        {
            get { return nbBalles; }
            set { nbBalles = value; }
        }

        public ObjetAnime ObjBalle
        {
            get { return uneBalle; }
            set { uneBalle = value; }
        }

        public Vector2 V_max
        {
            get { return v_max; }
            set { v_max = value; }

        }

        public Vector2 V_min
        {
            get { return v_min; }
            set { v_min = value; }
        }

        public Vector2 VitesseInitiale
        {
            get { return vitesse_initiale; }
            set { vitesse_initiale = value; }
        }

        //fonction testant si la balle � touch� le mur de droite, de gauche ou du haut
        private void collisionMur()
        {
            Vector2 v;

            if(uneBalle.Position.X  < minX || uneBalle.Position.X + uneBalle.Size.X > maxX)
            {
                
                v = uneBalle.Vitesse;
                //v.Y *= -1;
                v.X *= -1;
                uneBalle.Vitesse = v;
            }
            else if(uneBalle.Position.Y < minY)
            {
                v = uneBalle.Vitesse;
                v.Y *= -1;
             
                //v.X *= -1;
                uneBalle.Vitesse = v;
            }
        }

        //fonction testant si la balle est sortie de la fenetre par le bas
        public Boolean balleSortie()
        {
            if (uneBalle.Position.Y + uneBalle.Size.Y + tailleBalle.Y  > maxY )
            {
                nbBalles--;
                return true;
            }
            return false;
        }
    }
}
