
        SpriteBatch spriteBatch;
        private int maxX;
        private int maxY;
        private int minX;
        private int minY;
        int nbBalles;


        private Vector2 v_min;
        private Vector2 v_max;
        private Vector2 vitesse_initiale = Vector2.Zero;
        private Vector2 position_initiale;

        private ObjetAnime uneBalle;
        private SoundEffect soundRaquette;
        private SoundEffect soundMur;

        private BoundingBox bbox;
        private Raquette raquette;

        public Balle(Game game, int maxX, int maxY, int minX, int minY)
            : base(game)
        {
            this.maxX = maxX;
            this.maxY = maxY;
            this.minX = minX;
            this.minY = minY;

            this.Game.Components.Add(this);
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            v_min = new Vector2(9, 6);
            v_max = new Vector2(25, 16);
            vitesse_initiale = v_min;
            position_initiale = new Vector2(490, 650);

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            base.Update(gameTime);
        }

        public int NbBalles
        {
            get { return nbBalles; }
            set { nbBalles = value; }
        }

        public ObjetAnime ObjBalle
        {
            get { return uneBalle; }
            set { uneBalle = value; }
        }
